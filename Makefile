pre-publish:
	pip install --upgrade setuptools wheel twine
	python setup.py sdist bdist_wheel

publish: pre-publish
	python -m twine upload dist/*
